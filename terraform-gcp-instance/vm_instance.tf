resource "google_compute_instance" "instance_gitlab_runner" {
  name = "vm-giltab-runner"
  machine_type = "e2-micro"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }
  
  metadata = {
    ssh-keys = "heri-dev:${file("~/.ssh/id_rsa.pub")}"
  }

  tags = [ "http-server" ]

  network_interface {
    network = "default"
    access_config {
      
    }
  }
}
