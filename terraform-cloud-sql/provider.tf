provider "google" {
  project = "project-herianto-binar"
  region = "asia-southeast2"
  zone = "asia=southeast2-c"
}

terraform {
  backend "gcs" {
    bucket = "platinum-terraform-state"
    prefix = "terraform/state"
  }
}

provider "random" {
  
}