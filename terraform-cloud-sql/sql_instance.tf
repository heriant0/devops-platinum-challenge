# Create Instance Postgres SQL
resource "google_sql_database_instance" "sql_instance" {
  name = "platinum-postgres"
  database_version = "POSTGRES_14"
  deletion_protection = false
  settings {
    tier = "db-f1-micro"
    activation_policy = "ALWAYS"
    disk_size = 10
    deletion_protection_enabled = false
    ip_configuration {
      ipv4_enabled = true
      private_network = "projects/project-herianto-binar/global/networks/default"
      authorized_networks {
        name = "Allow Internet"
        value = "0.0.0.0/0"
      }
    }
  }
}

