resource "google_storage_bucket" "sql_bucket" {
  name = "platinum-bucket"
  location = "asia-southeast2"
}

# Upload to gcs
resource "google_storage_bucket_object" "sql-bucket-object" {
  name = "crud_db.sql"
  bucket = google_storage_bucket.sql_bucket.name
  source = "../database/crud_db.sql"
}

# Create bucket permission
resource "google_storage_bucket_iam_member" "bucket_permission" {
  bucket = google_storage_bucket.sql_bucket.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
  depends_on = [google_storage_bucket.sql_bucket]
}