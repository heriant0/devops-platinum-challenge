# Import query sql
resource "null_resource" "import_sql_query_staging" {
  provisioner "local-exec" {
    environment = {
      CLOUD_SQL_PASSWORD = "${google_sql_user.sql_user_staging.password}"
    }
    command = "gcloud sql import sql ${google_sql_database_instance.sql_instance.name} gs://platinum-bucket/crud_db.sql --database=${google_sql_database.sql_db_staging.name} --user=${google_sql_user.sql_user_staging.name}"
  }

  depends_on = [
    google_sql_database_instance.sql_instance,
    google_sql_user.sql_user_staging,
  ]
}

resource "null_resource" "import_sql_query_production" {
  provisioner "local-exec" {
    environment = {
      CLOUD_SQL_PASSWORD = "${google_sql_user.sql_user_production.password}"
    }
    command = "gcloud sql import sql ${google_sql_database_instance.sql_instance.name} gs://platinum-bucket/crud_db.sql --database=${google_sql_database.sql_db_production.name} --user=${google_sql_user.sql_user_production.name}"
  }

  depends_on = [
    google_sql_database_instance.sql_instance,
    google_sql_user.sql_user_production,
  ]
}