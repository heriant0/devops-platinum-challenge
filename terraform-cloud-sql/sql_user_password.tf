# Define user password
resource "random_id" "sql_user_password_staging" {
  byte_length = 8
}

resource "random_id" "sql_user_password_production" {
  byte_length = 10
}