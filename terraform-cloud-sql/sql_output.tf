output "sql_public_ip" {
  description = "Expose Public IP SQL Instance"
  value = google_sql_database_instance.sql_instance.public_ip_address
}

output "sql_private_ip" {
  description = "Expose Private IP SQL Instance"
  value = google_sql_database_instance.sql_instance.private_ip_address
}

output "staging_db_name" {
  description = "Expose staging database name"
  value = google_sql_database.sql_db_staging.name
}

output "staging_db_username" {
    description = "Expose staging database username"
    value = google_sql_user.sql_user_staging.name
}

output "staging_db_password" {
  description = "Expose SQL staging db password"
  value = random_id.sql_user_password_staging.id
}

output "production_db_name" {
  description = "Expose production database name"
  value = google_sql_database.sql_db_production.name
}

output "production_db_username" {
    description = "Expose production database username"
    value = google_sql_user.sql_user_production.name
}


output "production_db_password" {
  description = "Expose SQL production db password"
  value = random_id.sql_user_password_production.id
}