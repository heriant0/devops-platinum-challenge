# Create Database
resource "google_sql_database" "sql_db_staging" {
  name = "people_staging"
  instance = google_sql_database_instance.sql_instance.name
}

resource "google_sql_database" "sql_db_production" {
  name = "people_production"
  instance = google_sql_database_instance.sql_instance.name
}