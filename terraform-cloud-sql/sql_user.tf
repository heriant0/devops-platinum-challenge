# Create user sql
resource "google_sql_user" "sql_user_staging" {
  name = "people_staging"
  password = random_id.sql_user_password_staging.id
  instance = google_sql_database_instance.sql_instance.name
}

resource "google_sql_user" "sql_user_production" {
  name = "people_production"
  password = random_id.sql_user_password_production.id
  instance = google_sql_database_instance.sql_instance.name
}