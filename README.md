
## Platinum Challenge
- [x] Menggunakan Google Kubernetes Engine untuk membuat Kubernetes Cluster
- [x] Memasang Ingress dengan Google Cloud Load Balancer
- [X] Implementasi domain + SSL untuk Ingress
- [x] Membuat dan menggunakan GitLab Runner di Kubernetes Cluster
- [x] Menggunakan Gitlab CI/CD Pipeline untuk melakukan deployment aplikasi ke Kubernetes Cluster
- [x] Membuat dashboard monitoring dan logging di GCP
- [x] Menggunakan Ansible untuk menginstal GitLab Runner
- [x] Menggunakan Terraform untuk membuat VM GitLab Runner dan Cloud SQL

## Diagram

![](assets/diagram.png)


## Setup Project
1. Create Intance Cloud SQL
    - Create database staging and production
    - Create user database staging and production

      ![](assets/sql_instance.png)

      ![](assets/sql_databases.png)

1. Kubernetes Cluster
    - Cluster

      ![](assets/kubernetes_cluster.png)

    - Service and Ingress

      ![](assets/services_and_ingress.png)

1. Kubernetes Runner 

    - Setup Runner
      
      ![](assets/kubernetes_runner.png)

    - CI/CD Deployment

      ![](assets/ci_cd_deployment.png)

1. Cloud Monitoring & Loging
    - Loging Cloud Load Balancer Staging (status 4xx and 5xx)

      ![](assets/logging_cloud_lb_staging.png)

    - Loging Cloud Load Balancer Production (status 4xx and 5xx)

      ![](assets/logging_cloud_lb_production.png)


    - GKE Cluster Monitoring

      ![](assets/gke_cluster_monitoring.png)

    - GKE Monitoring

      ![](assets/gke_monitoring.png)

1. VM Intance for Runner (Ansible + Gitlab runner)

    - GKE Monitoring

      ![](assets/vm_gitlab_runner.png)

    - GKE Monitoring

      ![](assets/ansible_gitlab_runner.png)

1. Result
    - Staging

    ![](assets/result_staging.png)

    - Production

    ![](assets/result_production.png)